import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Filter} from "../shared/constants/filter.enum";

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {
  filter = Filter;
  @Input() currentFilter: Filter;
  @Output() change: EventEmitter<Filter> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  isActive(filter: Filter) {
    return this.currentFilter === filter;
  }

  setFilter(filter: Filter) {
    this.change.emit(filter);
  }
}
