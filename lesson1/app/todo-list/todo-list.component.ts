import { Component, OnInit } from '@angular/core';
import {ITodo} from '../shared/model/itodo';
import {TodoStatus} from "../shared/constants/todo-status.enum";
import {Filter} from "../shared/constants/filter.enum";



@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  label: string = '';

  todos: ITodo[] = [
    {
      name: 'Позвонить в сервис',
      status: TodoStatus.TODO,
      time: new Date()
    },
    {
      name: 'Купить хлеб',
      status: TodoStatus.TODO
    },
    {
      name: 'Захватить мир',
      status: TodoStatus.DONE
    },
    {
      name: 'Добавить тудушку в список',
      status: TodoStatus.TODO
    }
  ];

  currentFilter: Filter = Filter.ALL;

  constructor() { }

  ngOnInit() {
  }

  submit() {
    const name = this.label;

    if (this.todos.every(todo => todo.name !== name)) {
      const newTodo = TodoListComponent.createNewTodo(name);
      this.todos = this.todos.concat([newTodo]);
      this.label = '';
    }
  }


  checkTodo(todo: ITodo) {
    switch (this.currentFilter) {
      case (Filter.ALL):
        return true;
      case (Filter.DONE):
        return todo.status === TodoStatus.DONE;
      case (Filter.TODO):
        return todo.status === TodoStatus.TODO;
    }
    return false;
  }

  changeTodoStatus(todoToChange: ITodo) {
    const status = todoToChange.status === TodoStatus.TODO ? TodoStatus.DONE : TodoStatus.TODO;

    this.todos = this.todos.map(todo => todo.name === todoToChange.name ? Object.assign({}, todo, {status}) : todo);
  }

  deleteTodo(todoToDelete: ITodo) {
    this.todos = this.todos.filter(
      todo => todo.name !== todoToDelete.name
    );
  }

  setCurrentFilter(filter: Filter) {
    this.currentFilter = filter;
  }

  static createNewTodo(name): ITodo {
    return {
      name,
      status: TodoStatus.TODO
    }
  }
}
