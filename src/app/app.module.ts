import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {CounterComponent} from './counter/counter.component';
import {TodoListComponent} from './todo-list/todo-list.component';
import {TodoItemComponent} from './todo-item/todo-item.component';
import {StatsComponent} from './stats/stats.component';
import {FiltersComponent} from './filters/filters.component';
import {Example1Component} from './di/example1/example1.component';
import {Example2Component} from './di/example2/example2.component';
import {DiSharedService} from "./di/di-shared.service";
import {DiNestedService} from "./di/di-nested.service";
import {TodosService} from "./shared/model/todos.service";
import { TemplateFormComponent } from './template-form/template-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import {ValidateEqual} from "./template-form/validate-equal.directive";

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    TodoListComponent,
    TodoItemComponent,
    StatsComponent,
    FiltersComponent,
    Example1Component,
    Example2Component,
    TemplateFormComponent,
    ReactiveFormComponent,
    ValidateEqual
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [
    DiSharedService,
    DiNestedService,
    TodosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
