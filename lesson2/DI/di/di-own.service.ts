import {Injectable} from '@angular/core';
import {DiNestedService} from "./di-nested.service";

@Injectable()
export class DiOwnService {

  constructor(private diNested: DiNestedService) {
    console.log('--- DiOwnService works!');
  }

}
