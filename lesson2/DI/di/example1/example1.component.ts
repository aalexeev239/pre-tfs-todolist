import {Component, OnInit} from '@angular/core';
import {DiSharedService} from "../di-shared.service";
import {DiOwnService} from "../di-own.service";
import {DiNestedService} from "../di-nested.service";

@Component({
  selector: 'app-example1',
  templateUrl: './example1.component.html',
  styleUrls: ['./example1.component.css'],
  providers: [DiOwnService, DiNestedService]
})
export class Example1Component implements OnInit {

  constructor(private diSharedService: DiSharedService,
              private diOwnService: DiOwnService) {
    const example = this.diSharedService;
  }

  ngOnInit() {
  }

}
