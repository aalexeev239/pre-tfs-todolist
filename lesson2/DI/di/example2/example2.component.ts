import {Component, OnInit} from '@angular/core';
import {DiSharedService} from "../di-shared.service";
import {DiOwnService} from "../di-own.service";
import {DiNestedService} from "../di-nested.service";

@Component({
  selector: 'app-example2',
  templateUrl: './example2.component.html',
  styleUrls: ['./example2.component.css'],
  providers: [DiOwnService, DiNestedService]
})
export class Example2Component implements OnInit {

  constructor(private diSharedService: DiSharedService,
              private diOwnService: DiOwnService) {
  }

  ngOnInit() {
  }

}
