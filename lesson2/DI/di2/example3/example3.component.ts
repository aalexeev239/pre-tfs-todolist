import {Component, OnInit} from '@angular/core';
import {DiSharedService} from "../di-shared.service";
import {DiOwnService} from "../di-own.service";
import {DiNestedService} from "../di-nested.service";

@Component({
  selector: 'app-example3',
  templateUrl: './example3.component.html',
  styleUrls: ['./example3.component.css']
})
export class Example3Component implements OnInit {

  constructor(private diSharedService: DiSharedService,
              private diOwnService: DiOwnService,
              private diNestedService: DiNestedService) {
  }

  ngOnInit() {
  }

}
