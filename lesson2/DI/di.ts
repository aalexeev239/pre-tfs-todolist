class Engine2 {
  public cylinders = 4;
}

class Tires2 {
  public make = "Pirelli";
}

class Car2 {
  public engine: Engine2;
  public tires: Tires2;

  public description = 'DI';

  constructor(engine: Engine2, tires: Tires2) {
    this.engine = engine;
    this.tires = tires;
  }

  // Method using the engine and tires
  drive() {
    return `${this.description} car with ` +
      `${this.engine.cylinders} cylinders and ${this.tires.make} tires.`;
  }
}

function main2() {
  const engine = new Engine2();
  const tires = new Tires2();
  const car = new Car2(engine, tires);

  car.drive();
}


function main3() {
  const injector = new Injector(...)
  const car = injector.get(Car);

  car.drive();
}


import {ReflectiveInjector} from '@angular/core';

function main4() {
  const injector = ReflectiveInjector.resolveAndCreate([
    Car2,
    Engine2,
    Tires2
  ]);

  // const injector = ReflectiveInjector.resolveAndCreate([
  //   {provide: Car2, useClass: Car2},
  //   {provide: Engine2, useClass: Engine2},
  //   {provide: Tires2, useClass: Tires2}
  // ]);

  const car = injector.get(Car);
}


import {Inject} from '@angular/core';

class Car3 {
  constructor(@Inject(Engine) engine,
              @Inject(Tires) tires,) {
    // ...
  }
}


class Car4 {
  constructor(engine: Engine,
              tires: Tires,) {
    // ...
  }
}
