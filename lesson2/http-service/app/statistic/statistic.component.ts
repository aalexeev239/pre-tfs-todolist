import { Component, OnInit, Input } from '@angular/core';
import {ITodo} from "../shared/model/itodo";
import {TodoStatus} from "../shared/constants/todo-status.enum";

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  @Input() todos: ITodo[];

  constructor() { }

  ngOnInit() {
  }

  get total() {
    return this.todos && this.todos.length;
  }

  get done() {
    return this.todos && this.todos.filter(({status}) => status === TodoStatus.DONE).length;
  }

  get todo() {
    return this.total - this.done;
  }
}
