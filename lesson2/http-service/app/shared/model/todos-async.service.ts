import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {ITodo} from "./itodo";
import {TodoStatus} from "../constants/todo-status.enum";

const BASE_URL = 'https://pre-todolist.firebaseio.com/todos';

@Injectable()
export class TodosAsyncService {

  constructor(private http: Http) {
  }

  getTodos(): Observable<ITodo[]> {
    return this.http.get(`${BASE_URL}.json`)
      .map((responce: Response)=> responce.json())
      .map(data => Object.keys(data).map(id => Object.assign({}, data[id], {id})))
  }

  addTodo(name: string) {
    return this.http.post(`${BASE_URL}.json`, TodosAsyncService.createNewTodo(name))
      .map((responce: Response)=> responce.json())
  }

  static createNewTodo(name: string): ITodo {
    return {
      id: '',
      name,
      status: TodoStatus.TODO
    }
  }
}
