import { Component, OnInit } from '@angular/core';
import {ITodo} from '../shared/model/itodo';
import {TodoStatus} from "../shared/constants/todo-status.enum";
import {Filter} from "../shared/constants/filter.enum";
import {TodosService} from "../shared/model/todos.service";



@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  label: string = '';
  todos: ITodo[];
  currentFilter: Filter = Filter.ALL;

  private todosService: TodosService;

  constructor(todosService: TodosService) {
    this.todosService = todosService;
  }

  ngOnInit() {
    this.updateTodos();
  }

  submit() {
    const isAdded = this.todosService.addTodo(this.label);

    if (isAdded) {
      this.label = '';
      this.updateTodos();
    }
  }


  checkTodo(todo: ITodo) {
    switch (this.currentFilter) {
      case (Filter.ALL):
        return true;
      case (Filter.DONE):
        return todo.status === TodoStatus.DONE;
      case (Filter.TODO):
        return todo.status === TodoStatus.TODO;
    }
    return false;
  }

  changeTodoStatus(todoToChange: ITodo) {
    this.todosService.changeTodoStatus(todoToChange);
    this.updateTodos();
  }

  deleteTodo(todoToDelete: ITodo) {
    this.todosService.deleteTodo(todoToDelete);
    this.updateTodos();
  }

  setCurrentFilter(filter: Filter) {
    this.currentFilter = filter;
  }

  private updateTodos() {
    this.todos = this.todosService.getTodos();
  }
}
