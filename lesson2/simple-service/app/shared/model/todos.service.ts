import { Injectable } from '@angular/core';
import {ITodo} from "./itodo";
import {TodoStatus} from "../constants/todo-status.enum";

@Injectable()
export class TodosService {

  todos: ITodo[] = [
    {
      name: 'Позвонить в сервис',
      status: TodoStatus.TODO,
      time: new Date()
    },
    {
      name: 'Купить хлеб',
      status: TodoStatus.TODO
    },
    {
      name: 'Захватить мир',
      status: TodoStatus.DONE
    },
    {
      name: 'Добавить тудушку в список',
      status: TodoStatus.TODO
    }
  ];

  constructor() { }

  getTodos() {
    return this.todos;
  }

  addTodo(name: string): boolean {
    if (!this.checkIfTodoExists(name)) {
      const newTodo = TodosService.createNewTodo(name);
      this.todos = this.todos.concat([newTodo]);
      return true;
    }

    return false;
  }

  changeTodoStatus(todoToChange: ITodo) {
    const status = todoToChange.status === TodoStatus.TODO ? TodoStatus.DONE : TodoStatus.TODO;
    this.todos = this.todos.map(todo => todo.name === todoToChange.name ? Object.assign({}, todo, {status}) : todo);
  }

  deleteTodo(todoToDelete: ITodo) {
    this.todos = this.todos.filter(
      todo => todo.name !== todoToDelete.name
    );
  }

  private checkIfTodoExists(name: string): boolean {
    return this.todos.some(todo => todo.name === name);
  }

  static createNewTodo(name: string): ITodo {
    return {
      name,
      status: TodoStatus.TODO
    }
  }
}
