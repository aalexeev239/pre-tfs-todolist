import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  title = 'app works!';
  counterValue = 0;

  counterChange(newValue) {
    this.counterValue = newValue;
  }
}
